productObject =
{
    remove: function() {
        var removeButtons = $('.removeProduct');

        removeButtons.unbind().click(function() {
            if ($("#listProduct tr").size() > 2) {
                $(this).parent().parent().remove();
            } else {
                $("#message").append('<div class="alert alert-danger" role="alert">"Não é possível apagar todos os produtos"</div>')
            }
        });
    },
    create : function(id, name, description, is_shipping, price) {
        var product = new Object;

        if (id == "") {
            id = null;
        }
        product.id = id;
        product.name = name;
        product.description = description;
        product.is_shipping = is_shipping;
        product.price = price;

        return product;
    },
    add : function()
    {
        var lastRowTable = $('#listProduct tr:last');
        lastRowTable.after(lastRowTable.clone().find("input").val("").end());
    }
};

$(function() {
    var addProduct = $('#new_product');
    var saveProducts = $('#save_products');
    var productListField = $("[name=products_serialize]");

    var form = $('form');

    productObject.remove();

    addProduct.click(function(){
        productObject.add()
        productObject.remove();
    });

    saveProducts.click(function() {
        var invalid = false;
        $('input:text').each(function() {
            if ($(this).val() == "") {
                invalid = true;
                return false;
            }
        });

        if (invalid) {
            $("#message div").remove();
            $("#message").append('<div class="alert alert-danger" role="alert">Existem campos vazios</div>');
            return false;
        }

        var productsList = new Array;
        $('#listProduct tr').each(function (index) {
            var name = $('[name^=name]').eq(index).val();
            if (name == undefined) {
                return true;
            }

            productNew = productObject.create(
                $('[name^=leroy_id]').eq(index).val(),
                name,
                $('[name^=description]').eq(index).val(),
                $('[name^=shipping]').eq(index).val(),
                $('[name^=price]').eq(index).val()
            );
            productsList.push(productNew);
        });

        productListField.val(JSON.stringify(productsList));
        form.submit();
    });
});
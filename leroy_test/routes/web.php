<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products', 'ProductsController@index');
Route::post('/products/upload', 'ProductsController@upload');
Route::get('/products/list', 'ProductsController@showProducts');
Route::get('/products/edit', 'ProductsController@edit');
Route::post('/products/update', 'ProductsController@update');

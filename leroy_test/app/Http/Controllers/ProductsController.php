<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Jobs\RewriteSheet;
use App\Models\ProductsImport;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Upload;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{
    private $destinationPath;

    public function __construct()
    {
        $this->destinationPath = config("app.sheet_dir");
    }

    public function index()
    {
        return view('products/index', []);
    }

    public function upload(Request $request)
    {
        $file = $request->file('file');

        if (is_null($file)) {
            $request->session()->flash('error', 'Arquivo não selecionado');
            return redirect('products');
        }

        if ($file->isValid() && (new Upload($file))->isAValidProductFile()) {
            $file->move($this->destinationPath, Product::PRODUCT_LIST_FILE . '.xls');
            $request->session()->flash('success', 'Importado com sucesso');
            return redirect('products');
        }

        $request->session()->flash('error', 'Arquivo inválido');

        return redirect('products');
    }

    public function showProducts()
    {
        $file = $this->getFullPathFileIfExists();
        $productImport = new ProductsImport(Excel::load($file));

        return view(
            'products/list',
            ['category' => $productImport->getCategory(), 'products' => $productImport->importProducts()]
        );
    }

    public function edit()
    {
        $file = $this->getFullPathFileIfExists();
        $productImport = new ProductsImport(Excel::load($file));

        return view(
            'products/edit',
            ['category' => $productImport->getCategory(), 'products' => $productImport->importProducts()]
        );
    }

    private function getFullPathFileIfExists()
    {
        $file = sprintf("%s/%s.xls", $this->destinationPath, Product::PRODUCT_LIST_FILE);
        if (!file_exists($file)) {
            return response("File does not exist", 500);
        }
        return $file;
    }

    public function update(Request $request)
    {
        $dataSerialized = $request->input('products_serialize');
        $category = $request->input('category');

        $job = (new RewriteSheet($dataSerialized, $category))->onConnection('redis');
        dispatch($job);

        $request->session()->flash('success', 'Atualização submetida a fila, atualize a página pra visualizar');
        return redirect('products/list');
    }
}

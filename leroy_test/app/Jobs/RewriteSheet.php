<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ProductsExport;

class RewriteSheet implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $serializedDataSheet;
    private $category;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $dataSheet, string $category)
    {
        $this->serializedDataSheet = $dataSheet;
        $this->category = $category;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product = new ProductsExport();
        $products = $product->getProductsFromJson($this->serializedDataSheet);
        $product->exportToXlS($products, $this->category);
    }
}

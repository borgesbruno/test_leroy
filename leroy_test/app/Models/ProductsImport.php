<?php

namespace App\Models;

use App\Entity;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

class ProductsImport
{
    private $rows;

    const LEROY_MERLIN_KEY = 1;
    const NAME_PRODUCT_KEY = 2;
    const SHIPPING_VALUE_KEY = 3;
    const DESCRIPTION_PRODUCT_KEY = 4;
    const PRICE_KEY = 5;

    public function __construct(LaravelExcelReader $excelReader)
    {
        $this->rows = $excelReader->get();
    }

    public function getCategory(): string
    {
        return $this->rows[0]->toArray()[1][2];
    }

    public function importProducts(): Entity\Products
    {
        $productsArray = array_slice($this->rows[0]->toArray(), 4);
        $products = new Entity\Products;

        foreach ($productsArray as $productItem) {
            if (is_null($productItem[self::LEROY_MERLIN_KEY])) {
                continue;
            }
            $products->addProduct(
                new Entity\Product(
                    $productItem[self::LEROY_MERLIN_KEY],
                    $productItem[self::NAME_PRODUCT_KEY],
                    $productItem[self::SHIPPING_VALUE_KEY],
                    $productItem[self::DESCRIPTION_PRODUCT_KEY],
                    (float) $productItem[self::PRICE_KEY]
                )
            );
        }

        return $products;
    }
}

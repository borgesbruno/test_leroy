<?php
namespace App\Models;

use App\Entity\Product;
use App\Entity\Products;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet as Worksheet;

class ProductsExport
{
    const BACKGROUND_COLOR = '#fcd5b5';
    const TOOLS = "Ferramentas";
    const ROW_START = 5;
    const EXPORT_PATH = "app/product_list";

    public static $lastId;

    public function getProductsFromJson($productsSerialized) : Products
    {
        $productsList = new Products;
        $arrayProducts = json_decode($productsSerialized);

        foreach ($arrayProducts as $productItem) {
            $product = new Product(
                $productItem->id,
                $productItem->name,
                $productItem->is_shipping,
                $productItem->description,
                (float) $productItem->price
            );
            $productsList->addProduct($product);
        }

        return $productsList;
    }

    public function exportToXlS(Products $products, $category = self::TOOLS)
    {
        Excel::create(Product::PRODUCT_LIST_FILE, function ($excel) use ($products, $category) {
            $excel->sheet('Plan1', function ($sheet) use ($products, $category) {
                $sheet->setAutoSize(true);
                $sheet->cells('A4:E4', function ($cell) {
                    $cell->setBackground(self::BACKGROUND_COLOR);
                });
                $sheet->cells('A2', function ($cell) {
                    $cell->setValue("category");
                    $cell->setBackground(self::BACKGROUND_COLOR);
                });
                $sheet->cells('B2', function ($cell) use ($category) {
                    $cell->setValue($category);
                });
                $sheet->row(4, [
                    "lm", "name", "free_shipping", "description", "price"
                ]);

                $row = self::ROW_START;
                foreach ($products as $product) {
                    $this->fillDataProduct($sheet, $product, $row);
                    $row++;
                }

                $sheet->setColumnFormat(array(
                    'E5:E10' => '0.00'
                ));
            });
            $excel->sheet('Plan2');
            $excel->sheet('Plan3');
        })->store('xls', storage_path(self::EXPORT_PATH));
    }

    private function fillDataProduct(Worksheet &$sheet, Product $product, $row)
    {

        if (!is_null($product->getLeroyMerlinId())) {
            $productId = $product->getLeroyMerlinId();
            self::$lastId = $productId;
        } else {
            $productId = ++self::$lastId;
        }

        $sheet->row($row, array(
            $productId,
            $product->getName(),
            $product->isFreeShipping(),
            $product->getDescription(),
            number_format($product->getPrice(), 2, '.', ',')
        ));
        $sheet->setHeight($row, 15);
    }
}

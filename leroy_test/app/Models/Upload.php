<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile as UploadFile;

class Upload extends Model
{
    private $uploadFile;

    private $validContenTypes = [
        'application/vnd.oasis.opendocument.spreadsheet',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.ms-excel'
    ];

    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    public function isAValidProductFile(): bool
    {
        $contentType = $this->uploadFile->getMimeType();
        return in_array($contentType, $this->validContenTypes);
    }
}

<?php

namespace App\Entity;

class Products extends \ArrayObject
{
    public function addProduct(Product $product)
    {
        $this->append($product);
    }
}
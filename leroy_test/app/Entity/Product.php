<?php

namespace App\Entity;

class Product
{
    private $name;
    private $freeShipping;
    private $description;
    private $price;
    private $leroyMerlinId;

    const FREE_SHIPPING = 1;
    const SHIPPING_WITH_COSTS = 0;
    const PRODUCT_LIST_FILE = 'product_list';

    public function __construct($leroyMerlinId, $name, $freeShipping, $description, $price)
    {
        if (!$this->isAValidPrice($price)) {
            throw new \UnexpectedValueException("Is not a valid price value");
        }

        if ($freeShipping != self::FREE_SHIPPING && $freeShipping != self::SHIPPING_WITH_COSTS) {
            throw new \UnexpectedValueException("Invalid shipping value");
        }

        $this->name = $name;
        $this->leroyMerlinId = $leroyMerlinId;
        $this->freeShipping = $freeShipping;
        $this->description = $description;
        $this->price = $price;
    }

    private function isAValidPrice($price): bool
    {
        return is_float($price);
    }

    public function getLeroyMerlinId()
    {
        return $this->leroyMerlinId;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function isFreeShipping(): bool
    {
        return (bool) $this->freeShipping;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}

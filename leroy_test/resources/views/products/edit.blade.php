<!DOCTYPE html>
<html>
<head>
    <title>Listagem de produtos</title>
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top leroy-bar">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Leroy Merlin Test</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/products">Home</a></li>
                <li><a href="/products/list">Produtos</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container body">
    <div class="content">
        <div class="title">
            <h1>Produtos</h1>
        </div>
        {!! Form::open(array('url'=>'/products/update','method'=>'POST', 'files'=>true, 'class'=> 'form-inline' )) !!}
        <p>Categoria: {{ Form::text('category', $category, ['class' => 'form-control']) }}</p>
        <div id="message"></div>
        <table id="listProduct" class="table">
            <tr>
                <th>Nome</th>
                <th>Frete grátis</th>
                <th>Descrição</th>
                <th>Preço</th>
                <th>Remover</th>
            </tr>
            @foreach ($products as $product)
                <tr>
                    <input type="hidden" name="leroy_id[]"  value="{{ $product->getLeroyMerlinId() }}">
                    <td>
                        {{ Form::text('name[]', $product->getName(), ['class' => 'form-control', 'required' => 'required']) }}
                    </td>
                    <td>
                        {{ Form::select('shipping[]', [0 => 'Não', 1 => 'Sim'], $product->isFreeShipping(), ['class' => 'form-control'])}}
                    </td>
                    <td>
                        {{ Form::text('description[]', $product->getDescription(), ['class' => 'form-control'])}}
                    </td>
                    <td>
                        {{ Form::number('price[]', number_format($product->getPrice(), 2), ['class' => 'form-control' ,'required' => 'required']) }}
                    </td>
                    <td class="actions">
                        <button type="button"class="removeProduct">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ Form::hidden('_token', csrf_token()) }}
        {{ Form::hidden('products_serialize', '') }}
        {!! Form::close() !!}

        {{ Form::button('Adicionar produto', ['id' => "new_product", 'class' => 'btn btn-info']) }}
        {{ Form::button('Salvar', ['id' => "save_products", 'class' => 'btn btn-success']) }}
    </div>
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
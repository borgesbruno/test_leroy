<!DOCTYPE html>
<html>
<head>
    <title>Listagem de produtos</title>
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top leroy-bar">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Leroy Merlin Test</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/products">Home</a></li>
                <li class="active"><a href="/products/list">Produtos</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container body">
    <div class="content">
        <div class="title">
            <h1>Produtos</h1>
        </div>
        <h3>Categoria: <span class="label label-info">{{$category}}</span></h3>
        <table class="table">
            <tr>
                <th>LM ID</th>
                <th>Nome</th>
                <th>Frete grátis</th>
                <th>Descrição</th>
                <th>Preço</th>
            </tr>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->getLeroyMerlinId() }}</td>
                    <td>{{ $product->getName() }}</td>
                    <td>{{ ($product->isFreeShipping()? "Sim" : "Não") }}</td>
                    <td>{{ $product->getDescription() }}</td>
                    <td>{{ number_format($product->getPrice(), 2) }}</td>
                </tr>
            @endforeach
        </table>
        <a href="/products/edit">
            {{ Form::button('Editar', ['id' => "save_products", 'class' => 'btn btn-success']) }}
        </a>
    </div>
    @if(Session::has('success'))
        <div>
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        </div>
    @endif
</div>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
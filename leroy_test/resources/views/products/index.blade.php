<!DOCTYPE html>
<html>
<head>
    <title>Produtos</title>
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top leroy-bar">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Leroy Merlin Test</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/products">Home</a></li>
                <li><a href="/products/list">Produtos</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container body home">
    <div class="content">
        <div class="title">
            <h1>Produtos</h1>
        </div>
        <p> Fazer upload do arquivo XLS </p>
        {!! Form::open(array('url'=>'/products/upload','method'=>'POST', 'files'=>true)) !!}
            <div class="form-group">
                {{ Form::label('file','Selecione o arquivo que deseja importar', array('id'=>'','class'=>'')) }}
                {{ Form::file('file','',array('id'=>'','class'=>'form-control')) }}
            </div>
            <div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{ Form::submit('Carregar', ['class' => 'btn btn-success']) }}
            </div>

            @if(Session::has('success'))
                <div>
                    <br>
                    <a href="/products/list">Ver produtos</a>
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    {{ Session::get('error') }}
                </div>
            @endif
        {!! Form::close() !!}
    </div>
</div>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
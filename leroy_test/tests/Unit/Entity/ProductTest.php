<?php

namespace Unit\Models;

use App\Entity\Product;

class ProductTest extends \TestCase
{

    public function testCreateAValidProduct()
    {
        $name = 'Furadeira';
        $description = "Furadeira muito boa!!";
        $price = 40.50;
        $lmId = 1234;

        $product = new Product(
            '1234',
            $name,
            Product::FREE_SHIPPING,
            $description,
            $price
        );

        $this->assertEquals($product->getDescription(), $description, "The description is wrong");
        $this->assertEquals($product->getName(), $name, "The description is wrong");
        $this->assertEquals($product->getPrice(), $price, "The value must be valid");
        $this->assertEquals($product->getLeroyMerlinId(), $lmId, "The id is wrong");
        $this->assertEquals($product->isFreeShipping(), Product::FREE_SHIPPING, "The value must be valid");
    }

    /**
     * @expectedException    \UnexpectedValueException
     */
    public function testWhenCreateAnInvalidPriceShouldThrowAnException()
    {
        $product = new Product(
            '1234',
            "Papel de parede",
            Product::FREE_SHIPPING,
            "Bom e barato",
            "INVALID_PRICE"
        );
    }

    /**
     * @expectedException    \UnexpectedValueException
     */
    public function testWhenCreateAnInvalidShippingValueShouldThrowAnException()
    {
        $product = new Product(
            '1234',
            "Papel de parede",
            89,
            "Bom e barato",
            45.00
        );
    }
}

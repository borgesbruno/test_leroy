<?php

namespace Unit\Models;

use App\Entity;
use App\Models;

class ProductsExportTest extends \TestCase
{
    private $productsExport;

    public function setUp()
    {
        $this->productsExport = new Models\ProductsExport();
    }

    public function testGetProductsFromJsonMustReturnAProductsList()
    {
        $jsonProducts = '[{"id":"1001","name":"Furadeira X","description":"Furadeira eficiente X",'
        . '"is_shipping":"0","price":"100.00"}]';

        $products = $this->productsExport->getProductsFromJson($jsonProducts);

        $this->assertInstanceOf('App\Entity\Products', $products);
        foreach ($products as $product) {
            $this->assertEquals('1001', $product->getLeroyMerlinId());
            $this->assertEquals('Furadeira X', $product->getName());
            $this->assertEquals("Furadeira eficiente X", $product->getDescription());
            $this->assertEquals(100.00, $product->getPrice());
            $this->assertEquals(0, $product->isFreeShipping());
        }
    }
}

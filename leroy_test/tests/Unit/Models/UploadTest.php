<?php

namespace Unit\Models;

use App\Models;
use Mockery;

class UploadTest extends \TestCase
{
    private $uploadStub;

    public function setUp()
    {
        $this->uploadStub = Mockery::mock('Illuminate\Http\UploadedFile');
    }

    public function testCheckAValidProductListFile()
    {
        $this->uploadStub->shouldReceive('getMimeType')
            ->andReturn('application/vnd.oasis.opendocument.spreadsheet');

        $uploadModel = new Models\Upload($this->uploadStub);
        $this->assertTrue($uploadModel->isAValidProductFile(), "This file must be a valid format");
    }

    public function testCheckAnInValidProductListFileS()
    {
        $this->uploadStub->shouldReceive('getMimeType')
            ->andReturn('application/msword');

        $uploadModel = new Models\Upload($this->uploadStub);
        $this->assertFalse($uploadModel->isAValidProductFile(), "This file must be a valid format");
    }

}

<?php

namespace Unit\Models;

use App\Entity;
use App\Models;
use Mockery;

class ProductsImportTest extends \TestCase
{
    private $productsImport;
    private $excelReader;
    private $rows;

    public function setUp()
    {
        $this->excelReader = Mockery::mock('Maatwebsite\Excel\Readers\LaravelExcelReader');
        $this->rows = Mockery::mock('Maatwebsite\Excel\Collections\RowCollection');
        $this->rows->shouldReceive('toArray')->andReturn($this->fixtureProducts());
        $sheetCollectionMock = [$this->rows];
        $this->excelReader->shouldReceive('get')->andReturn($sheetCollectionMock);
    }

    public function testGetCategoryShouldReturnTheRightCategory()
    {
        $this->productsImport = new Models\ProductsImport($this->excelReader);

        $category = $this->productsImport->getCategory();

        $this->assertNotEmpty($category, 'The category can not be empty');
        $this->assertInternalType('string', $category);
        $this->assertEquals($category, 'Ferramentas', 'The category is invalid');
    }

    public function testImportProductsShouldReturnAProductsCollection()
    {
        $this->productsImport = new Models\ProductsImport($this->excelReader);

        $products = $this->productsImport->importProducts();

        $this->assertInstanceOf('App\Entity\Products', $products, "This must return an product collection");

        $product = $products->offsetGet(0);

        $this->assertEquals('1001', $product->getLeroyMerlinId());
        $this->assertEquals('Furadeira X', $product->getName());
        $this->assertEquals("Furadeira super eficiente X", $product->getDescription());
        $this->assertEquals(140, $product->getPrice());
        $this->assertEquals(1, $product->isFreeShipping());
    }

    public function testImportProductsCannotImportAnEmptyProduct()
    {
        $this->productsImport = new Models\ProductsImport($this->excelReader);

        $products = $this->productsImport->importProducts();

        $this->assertFalse($products->offsetExists(2), "Empty products can not to be in the list");
    }

    private function fixtureProducts()
    {
        return [
            [],
            [1 => 'category', 2 => 'Ferramentas'],
            [],
            ['lm', 'name', 'free_shipping', 'description', 'description', 'price'],
            [
                1 => 1001,
                2 => 'Furadeira X',
                3 => true,
                4 => 'Furadeira super eficiente X',
                5 => 140
            ],
            [
                1 => 1002,
                2 => 'Chave de Fenda X',
                3 => true,
                4 => 'Chave de fenda simples',
                5 => 140
            ],
            [
                1 => null,
                2 => null,
                3 => null,
                4 => null,
                5 => null
            ]
        ];
    }
}

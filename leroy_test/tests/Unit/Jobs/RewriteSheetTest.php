<?php

namespace Unit\Job;

use App\Jobs\RewriteSheet;
use Illuminate\Events\Dispatcher;

class RewriteSheetTest extends \TestCase
{
    public function testRewriteSheetJob()
    {
        $data = '[{"id":"1001","name":"Furadeira X","description":"Furadeira eficiente X",'
                . '"is_shipping":"0","price":"100.00"}]';

        $this->expectsJobs(RewriteSheet::class);

        $job = new \App\Jobs\RewriteSheet($data, 'Ferramentas');
        app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($job);
    }
}
